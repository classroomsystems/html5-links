package main

import (
	"encoding/csv"
	"flag"
	"fmt"
	"io"
	"os"
	"strings"

	"golang.org/x/net/html"
	"golang.org/x/net/html/atom"
)

func main() {
	flag.Parse()
	errors := false
	w := csv.NewWriter(os.Stdout)
	for _, fname := range flag.Args() {
		f, err := os.Open(fname)
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
			errors = true
			continue
		}
		defer f.Close()
		err = fileLinks(w, f, fname)
		if err != nil {
			fmt.Fprintf(os.Stderr, "%s: %v\n", f, err)
			errors = true
		}
	}
	w.Flush()
	if err := w.Error(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		errors = true
	}
	if errors {
		os.Exit(1)
	}
}

func fileLinks(w *csv.Writer, r io.Reader, fname string) error {
	doc, err := html.Parse(r)
	if err != nil {
		return err
	}
	var hrefs []string
	var f func(n *html.Node) error
	f = func(n *html.Node) error {
		var err error
		if n.Type == html.ElementNode {
			hrefs = hrefs[:0]
			switch n.DataAtom {
			case atom.A, atom.Area, atom.Base, atom.Link:
				hrefs = append(hrefs, getAttributeVal(n, "href"))
			case atom.Audio, atom.Embed, atom.Iframe, atom.Img, atom.Script, atom.Track, atom.Video:
				hrefs = append(hrefs, getAttributeVal(n, "src"))
			case atom.Form:
				hrefs = append(hrefs, getAttributeVal(n, "action"))
			case atom.Object:
				hrefs = append(hrefs, getAttributeVal(n, "data"))
				for _, attr := range []string{"archive", "classid", "codebase"} {
					val := getAttributeVal(n, attr)
					if val != "" {
						fmt.Fprintf(os.Stderr, "%s: using obsolete attribute object[%s]\n", fname, attr)
					}
				}
			case atom.Source:
				hrefs = append(hrefs, getAttributeVal(n, "src"))
				if s := strings.Split(getAttributeVal(n, "srcset"), ","); len(s) > 0 {
					hrefs = append(hrefs, s...)
				}
			case atom.Applet, atom.Bgsound, atom.Frame, atom.Frameset, atom.Image:
				fmt.Fprintf(os.Stderr, "%s: using obsolete tag %s\n", fname, n.Data)
			}
			for _, href := range hrefs {
				if href == "" {
					continue
				}
				err = w.Write([]string{fname, n.Data, href})
				if err != nil {
					return err
				}
			}
		}
		for c := n.FirstChild; c != nil; c = c.NextSibling {
			err = f(c)
			if err != nil {
				return err
			}
		}
		return nil
	}
	return f(doc)
}

func getAttributeVal(n *html.Node, key string) string {
	for _, a := range n.Attr {
		if a.Key == key {
			return a.Val
		}
	}
	return ""
}
